
const INITIAL_STATE = {
    patients: [],
};


const stateReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'STATE':
            return {...state, ...action.value};
        default:
            return state;
    }
};
export default stateReducer;
