import {applyMiddleware, combineReducers, createStore, compose} from 'redux';
import thunkMiddleware from 'redux-thunk';
import logger from 'redux-logger';
import {persistStore, persistReducer, persistCombineReducers} from 'redux-persist';
import storage from 'redux-persist/lib/storage'
import {createWhitelistFilter} from 'redux-persist-transform-filter';
import thunk from 'redux-thunk';

// Reducers
import stateReducer from './reducers/StateReducer';

const saveSubsetWhitelistFilter = createWhitelistFilter('', ['auth']);


const persistConfig = {
    key: 'primary',
    storage: storage,
    transform: [saveSubsetWhitelistFilter],
    blacklist: [],
};

const rootReducer = persistCombineReducers(persistConfig, {
    state: stateReducer
});




export const store = createStore(rootReducer,applyMiddleware(thunk));
export const persistor = persistStore(store);
