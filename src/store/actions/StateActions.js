import {store} from '../../../src/store/store';
function data(res) {
    store.dispatch({
        type: 'STATE',
        value: res,
    });
}
export default data;
