import React from 'react';
import {render} from "react-dom";
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {store, persistor} from './store/store';


import MainScreen from './screen/MainScreen';
import PatientsScreen from './screen/PatientsScreen';


import {
    BrowserRouter,
    Routes,
    Route
} from "react-router-dom";



render(
    <BrowserRouter>
        <Provider store={store}>
            <PersistGate
                persistor={persistor}
                loading={
                    <div>
                        ЗАГРУЗКА !!!!
                    </div>
                }>
                <Routes>
                    <Route path="/" element={<MainScreen />} />
                    <Route path="/patients" element={<PatientsScreen />} />
                </Routes>
            </PersistGate>
        </Provider>
    </BrowserRouter>,
    document.getElementById('root')
);


// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
