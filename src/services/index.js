import axios from 'axios';
import {API_PATH} from '../config';


export async function request(request, data = true) {
    request = Object.assign({
        method: 'GET',
        url: '',
        auth: {
            username: 'Gais00',
            password: '12345678'
        }
    }, request);
    request.url = API_PATH + request.url;
    return await axios(request).then((response) => {
        return response.data;
    }).catch(function (error) {
        return [];
    });
}

export async function CreatePatients(data) {
    return await request({
        method: 'POST',
        url: `/customers/`,
        data: data
    });
}

export async function GetPatients() {
    return await request({
        method: 'GET',
        url: `/customers/`,
    });
}


export async function GetClients() {
    return await request({
        method: 'GET',
        url: `./input_data_5000/NeRelog_clients.json`,
    });
}


