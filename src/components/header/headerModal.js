import React from 'react';

import {
    BrowserRouter as Router,
    Switch,
    Route,
    NavLink,
    Link
} from "react-router-dom";
//
const Header = ({children, close}) => {
    return (
        <>
            <div className={'container-main container-main-modal container-main-header'}>
                <div className={'main-box'}>
                    <p className={'text text-bold text-color1 text-s24'}>{children}</p>
                </div>
                <button onClick={close} className={'btn btn-close'}>
                    <img src="/img/eva_close-fill.png" className={'icon icon-close'} alt=""/>
                </button>
            </div>
        </>
    )
}
export default Header;