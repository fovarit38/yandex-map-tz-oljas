import React from 'react';
import Select from 'react-select'

const Button = ({name, placeholder, options = []}) => {
    return (
        <>
            <Select className={'select select-def text text-s16'} placeholder={<div>{name}</div>}
                    options={options}/>
        </>
    )
}
export default Button;