import React from 'react';

const Button = ({name, placeholder, type = "text", maxlength = 255, nameForm, value, handleChange}) => {
    return (
        <>
            <div className={'input'}>
                <p className={'input_name text text-bold text-s13 text-color05'}>
                    {name}
                </p>
                <input name={nameForm} maxlength={maxlength} onChange={handleChange} value={value}
                       placeholder={placeholder}
                       className={'input_in text text-s16'}
                       type={type}/>
            </div>
        </>
    )
}
export default Button;