import React from 'react';

import {
    BrowserRouter as Router,
    Switch,
    Route,
    NavLink,
    Link
} from "react-router-dom";

const Layout = ({children}) => {
    return (
        <>
            <div className={'main-box'}>
                <div className={'siteBar'}>
                    <img src="/img/logo.png" className={'logo'} alt=""/>

                    <nav>
                        <NavLink activeClassName="active" className={"nav-link nav-link-calendar text text-s16"} to="/">
                            Календарь
                        </NavLink>
                        <NavLink activeClassName="active" className={"nav-link nav-link-patients text text-s16"}
                                 to="/patients">
                            Пациенты
                        </NavLink>
                    </nav>

                </div>
                <main>{children}</main>
            </div>
        </>
    )
}
export default Layout;