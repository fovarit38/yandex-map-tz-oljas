import '../assets/scss/app.scss';

import React, {useEffect, useState, useRef, forwardRef} from 'react';

import {CreatePatients, GetApps, GetClients, GetPatients} from '../services';
import {connect} from 'react-redux';
import ActivityIndicator from 'react-activity-indicator';

import State from "../store/actions/StateActions";

import Layout from "../components/layout/Layout";
import Header from "../components/header/header";
import HeaderModal from "../components/header/headerModal";
import InputCustom from "../components/input/input";
import SelectCustom from "../components/input/select";

import Modal from 'react-modal';

function App({patients}) {

    const [modalIsOpen, setIsOpen] = React.useState(false);
    const [reload, setReload] = useState(false);
    const [sendForm, setSendForm] = useState(false);

    const [first_name, set_first_name] = React.useState("");
    const [last_name, set_last_name] = React.useState("");
    const [patronymic, set_patronymic] = React.useState("");
    const [birth_date, set_birth_date] = React.useState("");
    const [iin, set_iin] = React.useState("");
    const [phone, set_phone] = React.useState("");


    async function getState() {
        State({
            patients: await GetPatients(),
        })
    }

    async function createPatients() {
        if (!sendForm) {

            await CreatePatients({
                first_name,
                last_name,
                patronymic,
                iin,
                phone,
                birth_date,
            });
            set_first_name("");
            set_last_name("");
            set_patronymic("");
            set_iin("");
            set_phone("");
            setIsOpen(false);
            setReload(!reload);
            setSendForm(false);
        }
    }

    function closeModal() {
        setIsOpen(false);
    }


    useEffect(() => {
        getState();
    }, [reload]);


    return (
        <Layout>

            <Modal
                className={"modalAdd"}
                isOpen={modalIsOpen}
                closeTimeoutMS={500}
                contentLabel="Example Modal"
            >
                <HeaderModal close={closeModal}>
                    Новый пациент
                </HeaderModal>

                <form className="inputs">

                    <InputCustom nameForm={"first_name"} value={first_name} handleChange={(text) => {
                        set_first_name(text.target.value)
                    }} name={"Имя"} placeholder={"Петр"}/>

                    <InputCustom nameForm={"last_name"} value={last_name} handleChange={(text) => {
                        set_last_name(text.target.value)
                    }} name={"Фамилия"} placeholder={"Первый "}/>

                    <InputCustom nameForm={"patronymic"} value={patronymic} handleChange={(text) => {
                        set_patronymic(text.target.value)
                    }} name={"Отчество"}
                                 placeholder={"Константинович"}/>

                    <InputCustom nameForm={"iin"} maxlength={12} handleChange={(text) => {
                        set_iin(text.target.value)
                    }} value={iin} name={"ИИН"} placeholder={"123456789012"}/>

                    <SelectCustom options={
                        [
                            {value: 'Муж', label: 'Муж'},
                            {value: 'Жен', label: 'Жен'},
                        ]
                    } name={"Пол"} placeholder={""}/>

                    <InputCustom type={"date"} nameForm={"birth_date"} handleChange={(text) => {
                        set_birth_date(text.target.value)
                    }} value={birth_date} name={"Дата рождения"} placeholder={"+7 (___) ___-__-__"}/>

                    <InputCustom nameForm={"phone"} handleChange={(text) => {
                        set_phone(text.target.value)
                    }} value={phone} name={"Телефон"} placeholder={"+7 (___) ___-__-__"}/>


                    <SelectCustom name={"Статус"} placeholder={""}/>
                    <InputCustom name={"Откуда пришел"} placeholder={"2gis"}/>
                </form>

                <div className="modal_footer">
                    <button className={'btn btn-cansel text text-16 text-bold text-color1'}>Отмена</button>
                    <button onClick={createPatients}
                            className={'btn btn-on text text-16 text-bold text-color1'}>

                        {
                            !sendForm && "Сохранить"
                        }
                        {
                            sendForm && (
                                <ActivityIndicator
                                    number={5}
                                    diameter={15}
                                    borderWidth={1}
                                    duration={200}
                                    activeColor="#66D9EF"
                                    borderColor="white"
                                    borderWidth={2}
                                    borderRadius="50%"
                                />
                            )
                        }

                    </button>
                </div>
            </Modal>

            <Header>
                Пациенты
            </Header>
            <div className={'container-main container-main-table'}>
                <div className="container-main_table">
                    <div className="table-head">
                        <button className={"btn btn-add text text-s14"} onClick={() => {
                            setIsOpen(true)
                        }}>Новый пациент
                        </button>
                    </div>
                    <table className="table-main">
                        <thead>
                        <tr>
                            <th className={'text text-s14'}></th>
                            <th className={'text text-color83 text-s14'}> Карточка</th>
                            <th className={'text text-color83 text-s14'}> Фамилия</th>
                            <th className={'text text-color83 text-s14'}> Имя</th>
                            <th className={'text text-color83 text-s14'}> Возраст</th>
                            <th className={'text text-color83 text-s14'}> Телефон</th>
                            <th className={'text text-color83 text-s14'}> Врач</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            patients && patients?.results.map((client) => {

                                return (
                                    <tr>
                                        <td className={'text text-s14'}></td>
                                        <td className={'text text-s14'}>{client?.card}</td>
                                        <td className={'text text-s14'}>{client?.last_name}</td>
                                        <td className={'text text-s14'}>{client?.first_name}</td>
                                        <td className={'text text-s14'}>{client?.birth_date}</td>
                                        <td className={'text text-s14'}>{client?.phone}</td>
                                    </tr>
                                )
                            })
                        }
                        </tbody>
                    </table>
                </div>
            </div>
        </Layout>
    );
}


const mapStateToProps = state => ({
    patients: state.state.patients,
});

export default connect(mapStateToProps)(App);
